#!/bin/bash -e

cd "$(dirname "$0")"

echo "New install" >install.log


echo "Creating new venv"
uv venv >>install.log

echo "Installing app"
uv pip install . >>install.log

CONFIG=false
if [[ ! -f domain_management/domain_management/local_settings.py ]]
then
    while true
    do
        read -rp "There is no previous local configuration. Do you want to copy the default and edit it? " yn
        case $yn in
            [Yy]* ) CONFIG=true; break;;
            [Nn]* ) echo "Not continuing, please add local_settings.py"; exit;;
            * ) echo "Please answer yes or no.";;
        esac
    done

    if [[ -z "${EDITOR}" ]]
    then
        # shellcheck disable=SC2016
        read -rp '$EDITOR is not specified, please enter the command for your editor: ' EDITOR
    fi

    cp domain_management/domain_management/local_settings.example.py domain_management/domain_management/local_settings.py
    $EDITOR domain_management/domain_management/local_settings.py
fi

echo "Applying migrations"
uv run --no-group dev domain_management/manage.py migrate  >>install.log
echo "Collecting static files"
uv run --no-group dev domain_management/manage.py collectstatic --no-input  >>install.log
echo "Removing stale contenttypes"
uv run --no-group dev domain_management/manage.py remove_stale_contenttypes --no-input  >>install.log

echo "Completed installation."
if "$CONFIG"
then
    INSTALL=false
    while true
    do
        read -rp "Since you are installing for the first time, do you want to run system/install_production.sh to configure systemd and nginx? " yn
        case $yn in
            [Yy]* ) INSTALL=true; break;;
            [Nn]* ) echo "If you want to do this at a later point, just run system/install_production.sh manually"; exit;;
            * ) echo "Please answer yes or no.";;
        esac
    done
    export INSTALL
    export EDITOR
    system/install_production.sh
fi

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms
from django.utils.translation import gettext_lazy as _

from dns.models import Host, ResourceRecord, Zone


class ZoneForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = "id_zoneForm"
        self.helper.form_method = "post"
        self.helper.title = "Zone"
        self.helper.add_input(Submit("save", "Save"))

    class Meta:
        model = Zone
        fields = "__all__"


class HostForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = "id_hostForm"
        self.helper.form_method = "post"
        self.helper.title = "Host"
        self.helper.add_input(Submit("save", "Save"))

    class Meta:
        model = Host
        fields = "__all__"


class ResourceRecordForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = "id_resourceRecordForm"
        self.helper.form_method = "post"
        self.helper.title = "Resource Record"
        self.helper.add_input(Submit("save", "Save"))

    class Meta:
        model = ResourceRecord
        fields = "__all__"


class FilterForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "get"
        self.helper.add_input(Submit("submit", _("Filter"), css_class="w-100"))

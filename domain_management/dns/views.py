from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, DetailView, UpdateView
from django_filters.views import FilterView
from django_tables2 import SingleTableMixin
from guardian.mixins import (
    LoginRequiredMixin,
    PermissionListMixin,
    PermissionRequiredMixin,
)

from dns.filters import (
    HostsFilter,
    ResourceRecordChildFilter,
    ResourceRecordFilter,
    ZoneFilter,
)
from dns.forms import HostForm, ResourceRecordForm, ZoneForm
from dns.models import Host, ResourceRecord, Zone
from dns.tables import (
    ChildHostsTable,
    ChildRecordTable,
    HostsTable,
    ResourceRecordTable,
    ZoneTable,
)


class ZoneDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Zone
    permission_required = "dns.view_zone"
    template_name = "dns/zone_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        child_zones = self.get_object().child_zones.all()
        child_zones_filter = ZoneFilter(
            self.request.GET, queryset=child_zones, prefix="zone"
        )
        context["childzones_filter"] = child_zones_filter
        context["childzones_table"] = ZoneTable(child_zones_filter.qs)

        hosts = self.get_object().hosts.all()
        hosts_filter = HostsFilter(self.request.GET, queryset=hosts, prefix="host")
        context["hosts_filter"] = hosts_filter
        context["hosts_table"] = ChildHostsTable(hosts_filter.qs)

        return context


class ZoneEditView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = Zone
    permission_required = "dns.change_zone"
    template_name = "generic/object_edit.html"
    form_class = ZoneForm


class ZoneListView(
    LoginRequiredMixin, PermissionListMixin, SingleTableMixin, FilterView
):
    model = Zone
    permission_required = "dns.view_zone"
    table_class = ZoneTable
    template_name = "dns/zone_list.html"
    filterset_class = ZoneFilter


class ZoneCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Zone
    permission_required = "dns.add_zone"
    permission_object = None
    accept_global_perms = True
    form_class = ZoneForm
    template_name = "generic/object_create.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["model_name"] = self.model.__name__
        return context

    def get_initial(self):
        zone_id = self.request.GET.get("parent_zone", None)
        initial = {"parent_zone": zone_id}
        return initial


class ZoneDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    model = Zone
    permission_required = "dns.delete_zone"
    success_url = reverse_lazy("dns:zone-list")
    template_name = "generic/object_confirm_delete.html"


class HostDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Host
    permission_required = "dns.view_host"
    template_name = "dns/host_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        child_records = self.get_object().all_records.all()
        child_records_filter = ResourceRecordChildFilter(
            self.request.GET, queryset=child_records, prefix="records"
        )
        context["filter"] = child_records_filter
        context["record_table"] = ChildRecordTable(child_records_filter.qs)
        return context


class HostEditView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = Host
    permission_required = "dns.change_host"
    form_class = HostForm
    template_name = "generic/object_edit.html"


class HostListView(
    LoginRequiredMixin, PermissionListMixin, SingleTableMixin, FilterView
):
    model = Host
    permission_required = "dns.view_host"
    table_class = HostsTable
    template_name = "dns/host_list.html"
    filterset_class = HostsFilter


class HostCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Host
    permission_required = "dns.add_host"
    permission_object = None
    form_class = HostForm
    template_name = "generic/object_create.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["model_name"] = self.model.__name__
        return context

    def get_initial(self):
        zone_id = self.request.GET.get("parent_zone", None)
        initial = {"parent_zone": zone_id}
        return initial


class HostDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    model = Host
    permission_required = "dns.delete_host"
    template_name = "generic/object_confirm_delete.html"

    def get_success_url(self) -> str:
        deleted_object = self.get_object()
        parent_id = deleted_object.parent_zone.id
        return reverse_lazy("dns:zone-detail", kwargs={"pk": parent_id})


class RecordEditView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = ResourceRecord
    permission_required = "dns.change_resourcerecord"
    form_class = ResourceRecordForm
    template_name = "generic/object_edit.html"


class RecordCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = ResourceRecord
    permission_required = "dns.add_resourcerecord"
    permission_object = None
    form_class = ResourceRecordForm
    template_name = "generic/object_create.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["model_name"] = self.model.__name__
        return context

    def get_initial(self):
        host_id = self.request.GET.get("host", None)
        initial = {"host": host_id}
        return initial


class RecordDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    model = ResourceRecord
    permission_required = "dns.delete_resourcerecord"
    template_name = "generic/object_confirm_delete.html"

    def get_success_url(self) -> str:
        deleted_object = self.get_object()
        host_id = deleted_object.host.id
        return reverse_lazy("dns:host-detail", kwargs={"pk": host_id})


class RecordListView(
    LoginRequiredMixin, PermissionRequiredMixin, SingleTableMixin, FilterView
):
    model = ResourceRecord
    permission_required = "dns.view_resourcerecord"
    template_name = "dns/record_list.html"
    table_class = ResourceRecordTable
    filterset_class = ResourceRecordFilter

import string

from django.core.exceptions import ValidationError


def validate_domain_name(name):
    allowed = set(string.ascii_letters + string.digits + "-._")
    if not set(name) <= allowed:
        raise ValidationError(f"Domain name {name} contains invalid characters")
    if name[0] in "-.":
        raise ValidationError("Domain names cannot begin with . or -")


def validate_fqdn(name):
    if name[:2] == "*.":
        name = name[2:]
    validate_domain_name(name)
    if name[-1] != ".":
        raise ValidationError(f"FQDNs must end with a . ({name})")
    if len(name) > 254:
        raise ValidationError(f"Domain name f{name} is too long")

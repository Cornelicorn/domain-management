from django.core.exceptions import (
    MultipleObjectsReturned,
    ObjectDoesNotExist,
    PermissionDenied,
)
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import authentication, permissions, viewsets
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)
from rest_framework.exceptions import NotFound, ParseError
from rest_framework.response import Response
from rest_framework.views import APIView

from dns.api.serializers import HostSerializer, ResourceRecordSerializer, ZoneSerializer
from dns.models import Host, ResourceRecord, Zone
from dns.utils import get_host_by_acme_fqdn, get_host_by_fqdn


class ZoneViewSet(viewsets.ModelViewSet):
    queryset = Zone.objects.all()
    serializer_class = ZoneSerializer


class HostViewSet(viewsets.ModelViewSet):
    queryset = Host.objects.all()
    serializer_class = HostSerializer


class ResourceRecordViewSet(viewsets.ModelViewSet):
    queryset = ResourceRecord.objects.all()
    serializer_class = ResourceRecordSerializer


class LegoACMEPresentView(APIView):
    authentication_classes = [authentication.BasicAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                "fqdn": openapi.Schema(type=openapi.TYPE_STRING, description="FQDN"),
                "value": openapi.Schema(type=openapi.TYPE_STRING, description="Token"),
            },
        )
    )
    def post(self, request, format=None):
        try:
            fqdn = request.data["fqdn"]
            rr_data = request.data["value"]
        except KeyError:
            raise ParseError("Could not determine fqdn and value from request")

        try:
            host = get_host_by_acme_fqdn(fqdn)
            has_acme_perm = request.user.has_perm("dns.verify_acme", host)
        except NotFound:
            has_acme_perm = False

        try:
            acme_host = get_host_by_fqdn(fqdn)
            has_direct_perm = request.user.has_perm("dns.change_host", acme_host)
            has_direct_acme_perm = request.user.has_perm("dns.verify_acme", acme_host)
        except NotFound:
            has_direct_perm = False
            has_direct_acme_perm = False

        if not (has_direct_perm or has_direct_acme_perm or has_acme_perm):
            raise PermissionDenied()

        try:
            acme_host = get_host_by_fqdn(fqdn)
        except NotFound:
            acme_host, _ = Host.objects.get_or_create(
                parent_zone=host.parent_zone, rr_name=f"_acme-challenge.{host.rr_name}"
            )

        try:
            ResourceRecord.objects.get(
                host=acme_host,
                rr_class="IN",
                rr_type="TXT",
                rr_data=rr_data,
                active=True,
            )
        except ObjectDoesNotExist:
            ResourceRecord.objects.create(
                host=acme_host,
                rr_class="IN",
                rr_type="TXT",
                rr_data=rr_data,
                comment="Created via Lego ACME endpoint",
            )
        except MultipleObjectsReturned:
            pass

        resp = {
            "status": "Successful",
            "fqdn": f"{fqdn}",
            "txt": f"{rr_data}",
        }
        return Response(resp, status=200)


class LegoACMECleanupView(APIView):
    authentication_classes = [authentication.BasicAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                "fqdn": openapi.Schema(type=openapi.TYPE_STRING, description="FQDN"),
                "value": openapi.Schema(type=openapi.TYPE_STRING, description="Token"),
            },
        )
    )
    def post(self, request, format=None):
        try:
            fqdn = request.data["fqdn"]
            rr_data = request.data["value"]
        except KeyError:
            raise ParseError("Could not determine fqdn and value from request")

        try:
            host = get_host_by_acme_fqdn(fqdn)
            has_acme_perm = request.user.has_perm("dns.verify_acme", host)
        except NotFound:
            has_acme_perm = False

        try:
            acme_host = get_host_by_fqdn(fqdn)
            has_direct_perm = request.user.has_perm("dns.change_host", acme_host)
            has_direct_acme_perm = request.user.has_perm("dns.verify_acme", acme_host)
        except NotFound:
            has_direct_perm = False
            has_direct_acme_perm = False

        try:
            acme_host = get_host_by_fqdn(fqdn)
        except NotFound:
            acme_host = Host.objects.get(
                parent_zone=host.parent_zone, rr_name="_acme-challenge"
            )

        if not (has_direct_perm or has_direct_acme_perm or has_acme_perm):
            raise PermissionDenied()

        try:
            records = ResourceRecord.objects.filter(
                host=acme_host,
                rr_class="IN",
                rr_type="TXT",
                rr_data=rr_data,
                active=True,
            )
            records.delete()
        except ObjectDoesNotExist:
            pass

        resp = {
            "status": "Successful",
            "fqdn": f"{fqdn}",
            "txt": f"{rr_data}",
        }
        return Response(resp, status=200)


@api_view(["GET", "POST"])
@permission_classes((permissions.IsAuthenticated,))
@authentication_classes((authentication.BasicAuthentication,))
@csrf_exempt
def DNSUpdateIPv4(request, fqdns, ip):
    status = 200
    user = request.user
    for fqdn in fqdns.split(","):
        host = get_host_by_fqdn(fqdn)
        created = None
        records = ResourceRecord.objects.filter(
            Q(host=host), Q(rr_class="IN"), Q(rr_type="A"), Q(active=True)
        )
        if len(records) == 1:
            record = records[0]
        elif len(records) == 0:
            if user.has_perm("dns.add_resourcerecord") or user.has_perm(
                "dns.change_host", host
            ):
                record, created = ResourceRecord.objects.get_or_create(
                    host=host, rr_class="IN", rr_type="A", rr_data=ip, active=True
                )
            else:
                raise PermissionDenied()
        else:
            # Keep first record
            record = records[0]
            # Delete others
            records.exclude(pk=record.id).delete()

        if not (
            user.has_perm("dns.change_resourcerecord", record)
            or user.has_perm("dns.change_host", host)
        ):
            raise PermissionDenied()
        # Update ip
        if not created:
            record.rr_data = ip
            record.full_clean()
            record.save()

    resp = {
        "status": "Successful",
        "ip": f"{record.rr_data}",
    }
    return Response(resp, status=status)

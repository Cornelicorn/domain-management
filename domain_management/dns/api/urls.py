from django.urls import include, path
from rest_framework import routers

from dns.api.views import (
    DNSUpdateIPv4,
    HostViewSet,
    LegoACMECleanupView,
    LegoACMEPresentView,
    ResourceRecordViewSet,
    ZoneViewSet,
)

router = routers.DefaultRouter()

router.register(r"zones", ZoneViewSet)
router.register(r"hosts", HostViewSet)
router.register(r"records", ResourceRecordViewSet)


urlpatterns = [
    path("acme/lego/present", LegoACMEPresentView.as_view(), name="lego-present"),
    path("acme/lego/cleanup", LegoACMECleanupView.as_view(), name="lego-cleanup"),
    path("ddns/ipv4/fqdns=<fqdns>&ip=<ip>", DNSUpdateIPv4, name="ddns-ipv4"),
    path("", include(router.urls)),
]

app_name = "dns-api"

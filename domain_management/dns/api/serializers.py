from rest_framework import serializers

from dns.models import Host, ResourceRecord, Zone


class ZoneSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="dns-api:zone-detail")
    parent_zone = serializers.HyperlinkedIdentityField(view_name="dns-api:zone-detail")

    class Meta:
        model = Zone
        fields = [
            "url",
            "zone_name",
            "name",
            "parent_zone",
            "default_ttl",
            "soa_mname",
            "soa_rname",
            "soa_refresh",
            "soa_retry",
            "soa_expire",
            "soa_negative_ttl",
            "comment",
        ]


class HostSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="dns-api:host-detail")
    parent_zone = serializers.HyperlinkedIdentityField(view_name="dns-api:zone-detail")

    class Meta:
        model = Host
        fields = ["url", "rr_name", "parent_zone", "active", "comment"]


class ResourceRecordSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name="dns-api:resourcerecord-detail"
    )
    host = serializers.HyperlinkedIdentityField(view_name="dns-api:host-detail")

    class Meta:
        model = ResourceRecord
        fields = [
            "url",
            "host",
            "rr_class",
            "rr_type",
            "rr_ttl",
            "rr_data",
            "active",
            "comment",
        ]

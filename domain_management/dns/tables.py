import django_tables2 as tables
from django_tables2.utils import A
from utilities.tables.columns import (
    DeleteColumn,
    EditColumn,
    HumanizedSecondsColumn,
    OpenColumn,
)

from dns.models import Host, ResourceRecord, Zone


class ZoneTable(tables.Table):
    open = OpenColumn("dns:zone-detail")
    edit = EditColumn("dns:zone-edit")
    default_ttl = HumanizedSecondsColumn()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.base_columns["zone_name"].verbose_name = "Name"
        self.base_columns["comment"].verbose_name = "Comment"
        self.base_columns["comment"].orderable = False

    class Meta:
        model = Zone
        fields = [
            "zone_name",
            "default_ttl",
            "name",
            "soa_mname",
            "soa_rname",
            "comment",
        ]


class HostsBaseTable(tables.Table):
    open = OpenColumn("dns:host-detail")
    edit = EditColumn("dns:host-edit")
    delete = DeleteColumn("dns:host-delete")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.base_columns["comment"].verbose_name = "Comment"
        self.base_columns["comment"].orderable = False


class ChildHostsTable(HostsBaseTable):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.base_columns["rr_name"].verbose_name = "Name"

    class Meta:
        model = Host
        fields = ["rr_name", "active", "comment"]


class HostsTable(HostsBaseTable):
    parent_zone = tables.LinkColumn("dns:zone-detail", args=[A("parent_zone.pk")])

    class Meta:
        model = Host
        fields = ["fqdn", "active", "parent_zone", "comment"]


class ChildRecordTable(tables.Table):
    edit = EditColumn("dns:record-edit")
    delete = DeleteColumn("dns:record-delete")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.base_columns["rr_class"].verbose_name = "Class"
        self.base_columns["rr_type"].verbose_name = "Type"
        self.base_columns["rr_ttl"].verbose_name = "TTL"
        self.base_columns["rr_data"].verbose_name = "Data"
        self.base_columns["comment"].verbose_name = "Comment"
        self.base_columns["comment"].orderable = False

    class Meta:
        model = ResourceRecord
        fields = ["rr_class", "rr_type", "rr_ttl", "rr_data", "active", "comment"]


class ResourceRecordTable(ChildRecordTable):
    class Meta:
        model = ResourceRecord
        fields = [
            "host.fqdn",
            "rr_class",
            "rr_type",
            "rr_ttl",
            "rr_data",
            "active",
            "comment",
        ]

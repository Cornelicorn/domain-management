from rest_framework.exceptions import NotFound

from dns.models import ACMEAlias, Host
from dns.validators import validate_fqdn


def convert_mail_to_rname(email: str) -> str:
    name, domain = email.split("@")
    name = name.replace(".", "\\.")
    domain = f"{domain}."
    return f"{name}.{domain}"


def get_host_by_fqdn(fqdn: str) -> Host:
    validate_fqdn(fqdn)
    aliases = [i for i in ACMEAlias.objects.all() if i.fqdn == fqdn]
    if len(aliases) > 1:
        raise NotFound("More than one ACME Alias matched")
    if len(aliases) == 1:
        return aliases[0].actual_host
    hosts = [i for i in Host.objects.all() if i.fqdn == fqdn]
    if len(hosts) == 0:
        raise NotFound("Could not find the specified host")
    elif len(hosts) > 1:
        raise NotFound("More than one host matched")
    host = hosts[0]
    return host


def get_host_by_acme_fqdn(fqdn: str) -> Host:
    return get_host_by_fqdn(fqdn.removeprefix("_acme-challenge."))

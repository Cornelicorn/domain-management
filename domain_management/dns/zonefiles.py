import os
import shutil
import time
from io import StringIO

from dns.models import Zone
from dns.utils import convert_mail_to_rname
from domain_management.settings import (
    RNDC_KEYFILE,
    RUN_RNDC_RELOAD,
    ZONEFILE_PATH_PREFIX,
)


def reload_rndc():
    if RUN_RNDC_RELOAD:
        os.system(f"rndc -k {RNDC_KEYFILE} freeze")
        os.system(f"rndc -k {RNDC_KEYFILE} reload")
        os.system(f"rndc -k {RNDC_KEYFILE} thaw")


def zonefile_path(zone):
    return ZONEFILE_PATH_PREFIX / f"{zone.domain_name}zone"


def generate_zonefile(zone, run_reload=RUN_RNDC_RELOAD):
    next_serial = int(time.time())
    current_serial = zone.latest_serial
    if current_serial:
        while next_serial <= current_serial:
            next_serial += 1
    # Do this so the signal is not activated, which would end in a recursive
    # loop on this function
    Zone.objects.filter(pk=zone.id).update(latest_serial=next_serial)
    output_path = zonefile_path(zone)
    output = StringIO()

    comment = zone.comment or ""
    if comment:
        comment = comment.replace("\n", "\n; ")
        comment = f"; {comment}"

    zone_header = (
        f"; {zone.zone_name}\n"
        f"; {zone.domain_name}\n"
        f"{comment}\n"
        f"$TTL  {zone.default_ttl}\n"
        f"@ IN  SOA {zone.soa_mname} {convert_mail_to_rname(zone.soa_rname)} (\n"
        f"    {next_serial} \t; Serial\n"
        f"    {zone.soa_refresh} \t; Refresh\n"
        f"    {zone.soa_retry} \t; Retry\n"
        f"    {zone.soa_expire} \t; Expire\n"
        f"    {zone.soa_negative_ttl} \t; Negative Cache TTL\n"
        f")\n\n"
    )
    output.write(zone_header)

    hosts = zone.hosts.filter(active=True).prefetch_related("all_records")
    for host in hosts:
        comment = host.comment
        if comment:
            comment = comment.replace("\n", "\n; ")
            comment = f"; {comment}\n"
            output.write(comment)
        for record in host.all_records.filter(active=True):
            output.write(f"{record.zonefile_entry()}\n")
        output.write("\n")
    with open(output_path, "w") as f:
        output.seek(0)
        shutil.copyfileobj(output, f)
    output.close()
    if run_reload:
        reload_rndc()


def generate_all_zonefiles():
    for zone in Zone.objects.all():
        generate_zonefile(zone, run_reload=False)
    reload_rndc()

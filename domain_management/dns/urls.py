from django.urls import path

from dns.views import (
    HostCreateView,
    HostDeleteView,
    HostDetailView,
    HostEditView,
    HostListView,
    RecordCreateView,
    RecordDeleteView,
    RecordEditView,
    RecordListView,
    ZoneCreateView,
    ZoneDeleteView,
    ZoneDetailView,
    ZoneEditView,
    ZoneListView,
)

urlpatterns = [
    path("zones/", ZoneListView.as_view(), name="zone-list"),
    path("zones/create/", ZoneCreateView.as_view(), name="zone-create"),
    path("zones/<slug:pk>/", ZoneDetailView.as_view(), name="zone-detail"),
    path("zones/<slug:pk>/edit", ZoneEditView.as_view(), name="zone-edit"),
    path("zones/<slug:pk>/delete", ZoneDeleteView.as_view(), name="zone-delete"),
    path("hosts/", HostListView.as_view(), name="host-list"),
    path("hosts/create", HostCreateView.as_view(), name="host-create"),
    path("hosts/<slug:pk>/", HostDetailView.as_view(), name="host-detail"),
    path("hosts/<slug:pk>/edit", HostEditView.as_view(), name="host-edit"),
    path("hosts/<slug:pk>/delete", HostDeleteView.as_view(), name="host-delete"),
    path("records/", RecordListView.as_view(), name="record-list"),
    path("records/create", RecordCreateView.as_view(), name="record-create"),
    path("records/<slug:pk>/edit", RecordEditView.as_view(), name="record-edit"),
    path("records/<slug:pk>/delete", RecordDeleteView.as_view(), name="record-delete"),
]

app_name = "dns"

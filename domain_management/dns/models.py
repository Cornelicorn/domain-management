import uuid

from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.validators import (
    MaxValueValidator,
    validate_ipv4_address,
    validate_ipv6_address,
)
from django.db import models
from django.urls import reverse
from simple_history.models import HistoricalRecords

from dns.validators import validate_fqdn

# Static configuration of resource record information
resource_record_classes = [
    "IN",
    "CH",
    "HS",
]
resource_record_class_choices = [(i, i) for i in resource_record_classes]

resource_record_types = {
    "A": 1,
    "AAAA": 28,
    "AFSDB": 18,
    "APL": 42,
    "CAA": 257,
    "CDNSKEY": 60,
    "CDS": 59,
    "CERT": 37,
    "CNAME": 5,
    "CSYNC": 62,
    "DHCID": 49,
    "DLV": 32769,
    "DNAME": 39,
    "DNSKEY": 48,
    "DS": 43,
    "EUI48": 108,
    "EUI64": 109,
    "HINFO": 13,
    "HIP": 55,
    "HTTPS": 65,
    "IPSECKEY": 45,
    "KEY": 25,
    "KX": 36,
    "LOC": 29,
    "MX": 15,
    "NAPTR": 35,
    "NS": 2,
    "NSEC": 47,
    "NSEC3": 50,
    "NSEC3PARAM": 51,
    "OPENPGPKEY": 61,
    "PTR": 12,
    "RRSIG": 46,
    "RP": 17,
    "SIG": 24,
    "SMIMEA": 53,
    "SOA": 6,
    "SRV": 33,
    "SSHFP": 44,
    "SVCB": 64,
    "TA": 32768,
    "TKEY": 249,
    "TLSA": 52,
    "TSIG": 250,
    "TXT": 16,
    "URI": 256,
    "ZONEMD": 63,
}
resource_record_type_choices = [(i, i) for i in resource_record_types.keys()]


class Zone(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    zone_name = models.CharField("Descriptive Zone Name", max_length=253)
    name = models.CharField("Domain Name", max_length=253)
    parent_zone = models.ForeignKey(
        "self",
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name="child_zones",
    )
    default_ttl = models.PositiveIntegerField(
        "Default TTL",
        help_text="Time for which a record is allowed to be cached",
        default=60,
    )

    soa_mname = models.CharField(
        "MNAME",
        help_text="FQDN of primary DNS source",
        max_length=254,
    )
    soa_rname = models.EmailField(
        "RNAME", help_text="Mail address of responsible contact"
    )
    soa_refresh = models.PositiveIntegerField(
        "REFRESH", help_text="Time until secondaries refresh", default=3600
    )
    soa_retry = models.PositiveIntegerField(
        "RETRY", help_text="Time until secondaries retry a failed refresh", default=900
    )
    soa_expire = models.PositiveIntegerField(
        "EXPIRE",
        help_text="Time for which an expired domain will be served by secondaries",
        default=1209600,
    )
    soa_negative_ttl = models.PositiveIntegerField(
        "Negative TTL", help_text="TTL for negative cache hits", default=30
    )

    comment = models.TextField("Comment inside Zonefile", null=True, blank=True)

    latest_serial = models.IntegerField(
        "Last serial number", blank=True, null=True, editable=False
    )

    history = HistoricalRecords()

    @property
    def domain_name(self):
        domain_name = self.name
        if self.parent_zone:
            domain_name = f"{domain_name}.{self.parent_zone.domain_name}"
        return domain_name

    def __str__(self):
        return f"{self.zone_name} ({self.domain_name})"

    def clean(self):
        if not self.parent_zone:
            try:
                existing_zone = Zone.objects.get(name=self.name)
                if not existing_zone == self:
                    raise ValidationError("The domain name already exists.")
            except ObjectDoesNotExist:
                pass
        if self.id:
            for host in self.hosts.all():
                host.clean()
        validate_fqdn(self.soa_mname)
        validate_fqdn(self.domain_name)

    def get_absolute_url(self):
        return reverse("dns:zone-detail", kwargs={"pk": self.id})

    class Meta:
        ordering = ["parent_zone__name", "name"]
        permissions = [
            ("verify_acme", "Can authenticate amce requests for all hosts in this zone")
        ]


class Host(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    parent_zone = models.ForeignKey(
        Zone, related_name="hosts", on_delete=models.CASCADE, verbose_name="Parent Zone"
    )
    active = models.BooleanField("Active", default=True)
    rr_name = models.CharField(
        "Resource Record Name",
        max_length=253,
    )
    comment = models.TextField("Comment inside Zonefile", null=True, blank=True)
    history = HistoricalRecords()

    @property
    def fqdn(self):
        if self.rr_name == "@":
            return f"{self.parent_zone.domain_name}"
        else:
            return f"{self.rr_name}.{self.parent_zone.domain_name}"

    @property
    def records(self):
        return self.all_records.filter(active=True).all()

    def __str__(self):
        return self.fqdn

    def clean(self):
        validate_fqdn(self.fqdn)
        existing_host = Host.objects.filter(
            parent_zone=self.parent_zone, rr_name=self.rr_name
        )
        if existing_host.count() > 1:
            raise ValidationError("The hostname already exists.")
        elif existing_host.count() == 0:
            pass
        elif existing_host[0] == self:
            pass
        else:
            raise ValidationError("The hostname already exists.")

    def get_absolute_url(self):
        return reverse("dns:host-detail", kwargs={"pk": self.id})

    class Meta:
        ordering = ["-active", "rr_name"]
        permissions = [("verify_acme", "Can authenticate amce requests for this host")]


class ResourceRecord(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    host = models.ForeignKey(Host, related_name="all_records", on_delete=models.CASCADE)
    active = models.BooleanField("Active", default=True)
    rr_class = models.CharField(
        "Resource Record Class",
        max_length=2,
        choices=resource_record_class_choices,
        default="IN",
        blank=True,
        null=True,
    )
    rr_type = models.CharField(
        "Resource Record Type",
        max_length=10,
        choices=resource_record_type_choices,
        default="A",
    )
    rr_ttl = models.PositiveBigIntegerField(
        "Resource Record TTL",
        null=True,
        blank=True,
        validators=[
            MaxValueValidator(4294967295),
        ],
    )
    # Max length is not valid everywhere, TXT are allowed to be allowed to be
    # split but to keep it simple, limit the length
    rr_data = models.TextField(
        "Resource Record Data",
        null=True,
        blank=True,
    )
    comment = models.TextField("Comment inside Zonefile", null=True, blank=True)
    history = HistoricalRecords()

    def __str__(self, fqdn=False):
        return self.zonefile_entry(use_fqdn=True)

    def clean(self):
        if self.rr_type == "A":
            validate_ipv4_address(self.rr_data)
        if self.rr_type == "AAAA":
            validate_ipv6_address(self.rr_data)
        if self.rr_type == "CNAME":
            existing_records = ResourceRecord.objects.filter(host=self.host)
            if existing_records.count() > 1:
                raise ValidationError(
                    "Cannot add a CNAME when there are other resource records"
                )
            elif existing_records.count() == 0:
                pass
            elif existing_records[0] == self:
                pass
            else:
                raise ValidationError(
                    "Cannot add a CNAME when there are other resource records"
                )
            validate_fqdn(self.rr_data)

    def get_absolute_url(self):
        host_id = self.host.id
        return reverse("dns:host-detail", kwargs={"pk": host_id})

    def zonefile_entry(self, use_fqdn=False):
        hostname = self.host.fqdn if use_fqdn else self.host.rr_name
        comment = f" ; {self.comment}" if self.comment else ""
        # To allow newlines in comments, they have to be commented out again
        comment = comment.replace("\n", "\n; ")
        return f"{hostname} {self.rr_ttl or ''} {self.rr_class} {self.rr_type} {self.rr_data or ''}{comment}"

    class Meta:
        ordering = ["-active", "host", "rr_type"]


class ACMEAlias(models.Model):
    """Hacky class to solve CNAME records on _acme-challenge
    Instead of `fqdn`, use `actual_host` in ACME views
    """

    fqdn = models.CharField("Incoming name", max_length=254, unique=True)
    actual_host = models.ForeignKey(
        Host,
        on_delete=models.CASCADE,
        related_name="acme_aliased_to",
        verbose_name="Actual host",
    )

    history = HistoricalRecords()

    def __str__(self):
        return f"{self.fqdn} to {self.actual_host.fqdn}"

    def clean(self):
        validate_fqdn(self.fqdn)

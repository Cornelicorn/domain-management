from django.contrib import admin
from guardian.admin import GuardedModelAdmin
from simple_history.admin import SimpleHistoryAdmin

from dns.models import ACMEAlias, Host, ResourceRecord, Zone


class ZoneAdmin(SimpleHistoryAdmin, GuardedModelAdmin):
    pass


class HostAdmin(SimpleHistoryAdmin, GuardedModelAdmin):
    pass


class ResourceRecordAdmin(SimpleHistoryAdmin, GuardedModelAdmin):
    pass


class ACMEAliasAdmin(SimpleHistoryAdmin, GuardedModelAdmin):
    pass


admin.site.register(Zone, ZoneAdmin)
admin.site.register(Host, HostAdmin)
admin.site.register(ResourceRecord, ResourceRecordAdmin)
admin.site.register(ACMEAlias, ACMEAliasAdmin)

from django_filters import FilterSet

from dns.forms import FilterForm
from dns.models import Host, ResourceRecord, Zone


class ZoneFilter(FilterSet):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.filters["parent_zone"].queryset = Zone.objects.filter(
            id__in=self.queryset.values_list("parent_zone", flat=True).distinct()
        )

    class Meta:
        model = Zone
        form = FilterForm
        fields = [
            "zone_name",
            "name",
            "soa_mname",
            "soa_rname",
            "comment",
            "parent_zone",
        ]


class ChildHostsFilter(FilterSet):
    class Meta:
        model = Host
        form = FilterForm
        fields = ["rr_name", "active", "comment"]


class HostsFilter(FilterSet):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.filters["parent_zone"].queryset = Zone.objects.filter(
            id__in=self.queryset.values_list("parent_zone", flat=True).distinct()
        )

    class Meta:
        model = Host
        form = FilterForm
        fields = [
            "rr_name",
            "active",
            "parent_zone",
            "comment",
        ]


class ResourceRecordChildFilter(FilterSet):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.filters["rr_class"].extra["choices"] = sorted(
            {
                (i, i)
                for i in self.queryset.values_list("rr_class", flat=True).distinct()
            }
        )
        self.filters["rr_type"].extra["choices"] = sorted(
            {(i, i) for i in self.queryset.values_list("rr_type", flat=True).distinct()}
        )

    class Meta:
        model = ResourceRecord
        fields = ["rr_class", "rr_type", "rr_ttl", "rr_data", "active", "comment"]
        form = FilterForm


class ResourceRecordFilter(ResourceRecordChildFilter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.filters["host"].queryset = Zone.objects.filter(
            id__in=self.queryset.values_list("host", flat=True).distinct()
        )

    class Meta:
        model = ResourceRecord
        fields = [
            "host",
            "rr_class",
            "rr_type",
            "rr_ttl",
            "rr_data",
            "active",
            "comment",
        ]
        form = FilterForm

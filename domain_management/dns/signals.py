from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver
from guardian.models import GroupObjectPermission, UserObjectPermission
from guardian.shortcuts import assign_perm, remove_perm

from dns.models import (
    Host,
    ResourceRecord,
    Zone,
)
from dns.zonefiles import generate_zonefile, zonefile_path

# Update Zonefiles


@receiver(post_save, sender=Zone, weak=False)
def update_zonefile_zone(sender, instance, **kwargs):
    generate_zonefile(instance)


@receiver(post_delete, sender=Zone, weak=False)
def remove_zonefile(sender, instance, **kwargs):
    zonefile_path(instance).unlink()


@receiver([post_save, post_delete], sender=Host, weak=False)
def update_zonefile_host(sender, instance, **kwargs):
    generate_zonefile(instance.parent_zone)


@receiver([post_save, post_delete], sender=ResourceRecord, weak=False)
def update_zonefile_record(sender, instance, **kwargs):
    generate_zonefile(instance.host.parent_zone)


# Propagate Object permissions
# from Zones to Child Zones and Hosts
# from Hosts to Records


@receiver(post_save, sender=UserObjectPermission, weak=False)
def propagate_user_zone_permissions(sender, instance, **kwargs):
    if not isinstance(instance.content_object, Zone):
        return
    if instance.permission.codename == "change_zone":
        for perm in [
            "dns.view_zone",
            "dns.change_zone",
            "dns.delete_zone",
            "dns.verify_acme",
        ]:
            assign_perm(
                perm,
                instance.user,
                instance.content_object.child_zones.all(),
            )
        for perm in [
            "dns.view_host",
            "dns.change_host",
            "dns.delete_host",
            "dns.verify_acme",
        ]:
            assign_perm(
                perm,
                instance.user,
                instance.content_object.hosts.all(),
            )
    elif instance.permission.codename == "verify_acme":
        assign_perm(
            "dns.verify_acme",
            instance.user,
            instance.content_object.child_zones.all(),
        )
        assign_perm(
            "dns.verify_acme",
            instance.user,
            instance.content_object.hosts.all(),
        )


@receiver(post_delete, sender=UserObjectPermission, weak=False)
def remove_user_zone_permissions(sender, instance, **kwargs):
    if not isinstance(instance.content_object, Zone):
        return
    if instance.permission.codename == "change_zone":
        for perm in [
            "dns.view_zone",
            "dns.change_zone",
            "dns.delete_zone",
            "dns.verify_acme",
        ]:
            remove_perm(
                perm,
                instance.user,
                instance.content_object.child_zones.all(),
            )
        for perm in [
            "dns.view_host",
            "dns.change_host",
            "dns.delete_host",
            "dns.verify_acme",
        ]:
            remove_perm(
                perm,
                instance.user,
                instance.content_object.hosts.all(),
            )
    elif instance.permission.codename == "verify_acme":
        remove_perm(
            "dns.verify_acme",
            instance.user,
            instance.content_object.child_zones.all(),
        )
        remove_perm(
            "dns.verify_acme",
            instance.user,
            instance.content_object.hosts.all(),
        )


@receiver(post_save, sender=GroupObjectPermission, weak=False)
def propagate_group_zone_permissions(sender, instance, **kwargs):
    if not isinstance(instance.content_object, Zone):
        return
    if instance.permission.codename == "change_zone":
        for perm in [
            "dns.view_zone",
            "dns.change_zone",
            "dns.delete_zone",
            "dns.verify_acme",
        ]:
            assign_perm(
                perm,
                instance.group,
                instance.content_object.child_zones.all(),
            )
        for perm in [
            "dns.view_host",
            "dns.change_host",
            "dns.delete_host",
            "dns.verify_acme",
        ]:
            assign_perm(
                perm,
                instance.group,
                instance.content_object.hosts.all(),
            )
    elif instance.permission.codename == "verify_acme":
        assign_perm(
            "dns.verify_acme",
            instance.group,
            instance.content_object.child_zones.all(),
        )
        assign_perm(
            "dns.verify_acme",
            instance.group,
            instance.content_object.hosts.all(),
        )


@receiver(post_delete, sender=GroupObjectPermission, weak=False)
def remove_group_zone_permissions(sender, instance, **kwargs):
    if not isinstance(instance.content_object, Zone):
        return
    if instance.permission.codename == "change_zone":
        for perm in [
            "dns.view_zone",
            "dns.change_zone",
            "dns.delete_zone",
            "dns.verify_acme",
        ]:
            remove_perm(
                perm,
                instance.group,
                instance.content_object.child_zones.all(),
            )
        for perm in [
            "dns.view_host",
            "dns.change_host",
            "dns.delete_host",
            "dns.verify_acme",
        ]:
            remove_perm(
                perm,
                instance.group,
                instance.content_object.hosts.all(),
            )
    elif instance.permission.codename == "verify_acme":
        remove_perm(
            "dns.verify_acme",
            instance.group,
            instance.content_object.child_zones.all(),
        )
        remove_perm(
            "dns.verify_acme",
            instance.group,
            instance.content_object.hosts.all(),
        )


@receiver(post_save, sender=UserObjectPermission, weak=False)
def propagate_user_host_permissions(sender, instance, **kwargs):
    if not isinstance(instance.content_object, Host):
        return
    if instance.permission.codename == "change_host":
        for perm in [
            "view_resourcerecord",
            "change_resourcerecord",
            "delete_resourcerecord",
        ]:
            assign_perm(
                perm,
                instance.user,
                instance.content_object.all_records.all(),
            )


@receiver(post_delete, sender=UserObjectPermission, weak=False)
def remove_user_host_permissions(sender, instance, **kwargs):
    if not isinstance(instance.content_object, Host):
        return
    if instance.permission.codename == "change_host":
        for perm in [
            "view_resourcerecord",
            "change_resourcerecord",
            "delete_resourcerecord",
        ]:
            remove_perm(
                perm,
                instance.user,
                instance.content_object.all_records.all(),
            )


@receiver(post_save, sender=GroupObjectPermission, weak=False)
def propagate_group_host_permissions(sender, instance, **kwargs):
    if not isinstance(instance.content_object, Host):
        return
    if instance.permission.codename == "change_host":
        for perm in [
            "view_resourcerecord",
            "change_resourcerecord",
            "delete_resourcerecord",
        ]:
            assign_perm(
                perm,
                instance.group,
                instance.content_object.all_records.all(),
            )


@receiver(post_delete, sender=GroupObjectPermission, weak=False)
def remove_group_host_permissions(sender, instance, **kwargs):
    if not isinstance(instance.content_object, Host):
        return
    if instance.permission.codename == "change_host":
        for perm in [
            "view_resourcerecord",
            "change_resourcerecord",
            "delete_resourcerecord",
        ]:
            remove_perm(
                perm,
                instance.group,
                instance.content_object.all_records.all(),
            )

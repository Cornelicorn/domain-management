from collections import OrderedDict

from django.shortcuts import redirect
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView

from domain_management.settings import (
    LOGIN_REDIRECT_URL,
    SOCIAL_AUTH_OIDC_OIDC_ENDPOINT,
)


class APIRootView(APIView):
    _ignore_model_permissions = True
    exclude_from_schema = True
    swagger_schema = None

    def get(self, request, format=None):
        return Response(
            OrderedDict(
                (
                    (
                        "dns",
                        reverse("dns-api:api-root", request=request, format=format),
                    ),
                )
            )
        )


def sso_login_or_redirect(request):
    if request.user.is_authenticated:
        return redirect(LOGIN_REDIRECT_URL)
    else:
        if SOCIAL_AUTH_OIDC_OIDC_ENDPOINT:
            return redirect(reverse("social:begin", args=["oidc"]))
        else:
            return redirect("accounts/login")

"""domain_management URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

import debug_toolbar
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import include, path, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view

from domain_management.forms import LoginForm
from domain_management.views import APIRootView, sso_login_or_redirect

openapi_info = openapi.Info(
    title="Domain Management API",
    default_version="v1",
)

schema_view = get_schema_view(
    openapi_info,
    public=False,
)

urlpatterns = [
    path("", include("social_django.urls", namespace="social")),
    path("", sso_login_or_redirect),
    path("admin/", admin.site.urls),
    path(
        "accounts/login/",
        auth_views.LoginView.as_view(
            authentication_form=LoginForm,
        ),
        name="login",
    ),
    path(
        "accounts/logout/",
        auth_views.LogoutView.as_view(),
        name="logout",
    ),
    path("__debug__/", include(debug_toolbar.urls)),
    path("dns/", include("dns.urls")),
    path("api/", APIRootView.as_view()),
    path("api/dns/", include("dns.api.urls")),
    path("api-auth/", include("rest_framework.urls")),
    path("api/docs/", schema_view.with_ui("swagger"), name="schema-swagger-ui"),
    path("api/redoc/", schema_view.with_ui("redoc"), name="schema-redoc"),
    re_path(
        r"^api/swagger(?P<format>.json|.yaml)$",
        schema_view.without_ui(),
        name="schema-json",
    ),
]

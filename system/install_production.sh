#!/bin/bash -e

SYSTEMD_USER=domain_management

SOURCE="$(dirname "$0")"

while ! id "$SYSTEMD_USER" >/dev/null 2>&1
do
    while true
    do
        read -rp "There does not seem to be a \"${SYSTEMD_USER}\" user, do you want to create it? (Answer no to change username or use different, existing user) " yn
        case $yn in
            [Yy]* ) useradd -d "/var/lib/${SYSTEMD_USER}" -m -r "${SYSTEMD_USER}"; break;;
            [Nn]* ) read -rp "Please enter the name of the user that should run the application: " SYSTEMD_USER; break;;
            * ) echo "Please answer yes or no.";;
        esac
    done
done
echo "Using user \"$SYSTEMD_USER\" for running the application."


if [[ ! -f "${SOURCE}/uwsgi.ini" ]]
then
    while true
    do
        read -rp "There is no previous uwsgi.ini configuration. Do you want to copy the default and edit it? " yn
        case $yn in
            [Yy]* ) break;;
            [Nn]* ) echo "Not continuing, please add uwsgi.ini manually."; exit;;
            * ) echo "Please answer yes or no.";;
        esac
    done

    if [[ -z "${EDITOR}" ]]
    then
        # shellcheck disable=SC2016
        read -rp '$EDITOR is not specified, please enter the command for your editor: ' EDITOR
    fi

    cp "${SOURCE}/uwsgi.example.ini" "${SOURCE}/uwsgi.ini"
    sed -i "s/DUMMYUSER/${SYSTEMD_USER}/g" "${SOURCE}/uwsgi.ini"
    $EDITOR "${SOURCE}/uwsgi.ini"
fi

if [[ ! -f "${SOURCE}/nginx.conf" ]]
then
    while true
    do
        read -rp "There is no previous nginx configuration. Do you want to copy the default and edit it? " yn
        case $yn in
            [Yy]* ) break;;
            [Nn]* ) echo "Not continuing, please add nginx.conf manually."; exit;;
            * ) echo "Please answer yes or no.";;
        esac
    done

    HOSTNAME=$(hostname -f) || HOSTNAME=$(hostname -s)
    read -rp "What is the domain name of your server? (Defaults to ${HOSTNAME}) " READ_HOSTNAME
    if [[ -n "$READ_HOSTNAME" ]]
    then
        HOSTNAME=$READ_HOSTNAME
    fi

    if [[ -z "${EDITOR}" ]]
    then
        # shellcheck disable=SC2016
        read -rp '$EDITOR is not specified, please enter the command for your editor: ' EDITOR
    fi

    cp "${SOURCE}/nginx.example.conf" "${SOURCE}/nginx.conf"
    sed -i "s/example.com/${HOSTNAME}/g" "${SOURCE}/nginx.conf"
    $EDITOR "${SOURCE}/nginx.conf"
fi

echo "Installing nginx configuration"
cp "${SOURCE}/nginx.conf" /etc/nginx/sites-available/domain_management.conf
echo "Disabling default nginx site"
rm -f /etc/nginx/sites-enabled/default
echo "Enabling nginx site"
ln -sf /etc/nginx/sites-available/domain_management.conf /etc/nginx/sites-enabled/domain_management.conf

if ! [ -f /etc/ssl/certs/dhparam.pem ]
then
    openssl dhparam -out /etc/ssl/certs/dhparam.pem 4096
fi

if ! [[ -f /etc/ssl/certs/${HOSTNAME}.pem && -f /etc/ssl/certs/${HOSTNAME}.key ]]
then
    while true
    do
        read -rp "There seems to be no certificate for the domain name, do you want to generate a selfsigned? " yn
        case $yn in
            [Yy]* ) SELFSIGN=true; break;;
            [Nn]* ) echo "Not continuing, please add a certificate manually."; exit;;
            * ) echo "Please answer yes or no.";;
        esac
    done

    if [[ "$SELFSIGN" == true ]]
    then
        openssl req -x509 \
            -newkey rsa:4096 \
            -nodes \
            -keyout "/etc/ssl/private/${HOSTNAME}.key" \
            -out "/etc/ssl/certs/${HOSTNAME}.pem" \
            -sha256 \
            -subj "/CN=${HOSTNAME}" \
            -addext "subjectAltName=DNS:${HOSTNAME}"
    fi
fi

echo "Copying systemd units"
cp "${SOURCE}"/*.service /etc/systemd/system
sed -i "s/DUMMYUSER/${SYSTEMD_USER}/g" /etc/systemd/system/domain-management.service
cp "${SOURCE}"/*.preset /etc/systemd/preset

echo "Reloading systemd configuration"
systemctl daemon-reload
echo "Enabling units for next restart"
systemctl preset-all --preset-mode=full

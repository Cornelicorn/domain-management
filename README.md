# Django Project for Domain Management

## Installation

Install prerequisites:

```bash
apt update && apt install -y \
    git \
    nginx-full \
    postgresql \
    libpq-dev
```

Clone the git repository:

```bash
git clone https://gitlab.com/Cornelicorn/domain-management.git /opt/domain_management
```

If you want to use postgresql (recommended), create a database.
Replace `SECUREPASSWORD` with a random character string

```bash
systemctl enable --now postgresql.service
sudo -u postgres /usr/bin/psql -c "CREATE USER domain_management WITH ENCRYPTED PASSWORD 'SECUREPASSWORD';"
sudo -u postgres /usr/bin/psql -c "CREATE DATABASE domain_management;"
sudo -u postgres /usr/bin/psql -c "GRANT ALL PRIVILEGES ON DATABASE domain_management TO domain_management;"
```

Run `install.sh` to create a venv and run migrations on the database.
The script will ask you questions on the way for configuration.

Then run `systemctl restart nginx.service domain-management.service`

## Updates

To update, update the git repository, run install.sh and restart the app.

```bash
cd /opt/domain_management
git pull
./install.sh
systemctl restart domain-management.service
```
